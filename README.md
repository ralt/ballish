# ballish

_A pretty fast code search tool_

Visit us on [our website](https://ballish.margaine.com)!

## Installation

See the [download page](https://ballish.margaine.com/download.html).

## Usage

See the [manual](https://ballish.margaine.com/manual.html).

## Contribute

See the [contribute page](https://ballish.margaine.com/contribute.html).

## License

[GPLv2](LICENSE).

## Roadmap

- Support deletions properly.
- Add an argument to merge custom yaml file to extend supported
  extensions.
- Potentially add search on Git commits.
